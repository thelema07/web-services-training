
const mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcryptjs');

const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        // The validaton fails when i disable the following require feature
        required: false,
        trim: true,
        minlength: 1,
        unique: true,
        validate: {
            validator: validator.isEmail,
            message: '{VALUE} is not a valid email'
        }
    },
    password: {
        type: String,
        required: false,
        minlength: 6
    },
    tokens: [{
        access: {
            type: String,
            required: true
        },
        token: {
            type: String,
            required: true
        }
    }]
});

UserSchema.methods.toJSON = function(){
    let user = this;
    let userObject = user.toObject();

    return _.pick(userObject,['_id','email']);
}

UserSchema.methods.generateAuthToken = function(){
    let user = this;
    let access = 'auth';
    var token = jwt.sign({_id: user._id.toHexString(),access},'thelema7').toString();
    console.log(token, '--- from token');
    // user.tokens.push([{access,token}]);
    // este elemento no hace bien el push a la base de datos
    user.tokens = user.tokens.concat([{access, token}]);
    // When we authenticate a token it must b sent through the x-auth headers using a get request
    return user.save().then(() => {
        return token;
    }).catch(err => console.log(err));
}

UserSchema.statics.findByToken = function(token){
    let User = this;
    let decoded;

    try{
        decoded = jwt.verify(token,'thelema7');
    } catch (error){
        return Promise.reject('Authentication is required');
    }

    return User.findOne({
        _id: decoded._id,
        'tokens.token': token,
        'tokens.access': 'auth'
    });
};

UserSchema.statics.findByCredentials = function (email,password){
    let User = this;

    return User.findOne({email}).then((user) => {
        if(!user){
            return Promise.reject();
        }

        return new Promise((resolve,reject) => {
            bcrypt.compare(password,user.password,(err,res) => {
                if(res){
                    resolve(user);
                } else {
                    resolve(reject);
                }
            })
        });
    });
}

UserSchema.pre('save', function(next){
    let user = this;

    if(user.isModified('password')){
        bcrypt.genSalt(10,(err,salt) => {
            bcrypt.hash(user.password,salt,(err,hash) => {
                user.password = hash;
                next();
            });
        });
    } else {
        next();
    }
});

module.exports = mongoose.model('User',UserSchema);
