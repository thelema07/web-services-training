const mongoose = require('mongoose');

const companySchema = mongoose.Schema({
    name: {type: String},
    url: {type: String},
    category: {type: String},
    founded: {type: Number},
    email: {type: String},
    phone: {type: String},
    description: {type: String}  
});

module.exports = mongoose.model('Company',companySchema);