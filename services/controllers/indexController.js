
const Company = require('../models/index');

exports.addCompany = (req,res,next) => {

    let newCompany = new Company({
        name: req.body.name,
        url: req.body.url,
        category: req.body.category,
        founded: req.body.founded,
        email: req.body.email,
        phone: req.body.phone,
        description: req.body.description
    });

    newCompany.save(req.body)
        .then(data => {
            res.status(200).send(data);
            console.log(data);
        })
        .catch(err => res.status(500).send(err));
};

exports.getCompaniesByName = (req,res,next) => {
    let companyName = req.params.name;

    Company.find({name: { 
        $regex: companyName,
        $options: 'i'}
    },{
        name:1,
        founded:1,
        category:1,
        _id:0
    })
        .then(data => {
            if(data == []){
                res.status(404);
                res.send('Company does not exist');
            }
            res.status(200).send(data);
        })
        .catch(err => res.status(501).send(err));
};

exports.getCompaniesByCategory = (req,res,next) => {
    let category = req.params.category;

    Company.find({
        category_code: {$regex: category,$options: 'i'}
    },{
        name:1,
        category_code:1,
        founded_year: 1,
        description: 1,
        _id:0   
    })
        .then(data => {
            if(data == []){
                res.status(404).send('Category does not exists');
            }
            res.status(200).send(data);
        })
        .catch(err => res.status(501).send(err));
};

exports.getCompaniesByFoundedYear = (req,res,next) => {
    let foundedYear = Number(req.params.founded);

    Company.find({
        founded_year: { $lte: foundedYear }
    },{
        name:1,
        category_code:1,
        description: 1,
        phone_number: 1,
        _id: 0
    })
        .then(data => {
            if(data == []){
                res.status(404).send('Query not found');
            }
            res.status(200).send(data);
        })
        .catch(err => res.status(501).send(err));
};