
const express = require('express');

const { addUser, getPrivateUser, logUser } = require('../controllers/userController');

const router = express.Router();

router.post('/adduser/',addUser);
// This new sintax makes my auth function reusable
router.get('/private',getPrivateUser,(req,res) => {
    res.send(req.user);
});

router.post('/login/',logUser);

module.exports = router;