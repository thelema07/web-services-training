/* NodeJs Modules */
const express = require('express');

const {getCompaniesByName, 
    addCompany, getCompaniesByCategory
    , getCompaniesByFoundedYear} = require('../controllers/indexController');

const router = express.Router();

router.get('/get-companies/:name',getCompaniesByName);
router.get('/get-categories/:category',getCompaniesByCategory);
router.get('/get-foundation/:founded',getCompaniesByFoundedYear);
router.post('/add-company/:name/:url/:category/:founded/:email/:phone/:description',addCompany);

module.exports = router;