
const bcrypt = require('bcryptjs');

let password = '123abc';
bcrypt.genSalt(10,(err,salt) => {
    bcrypt.hash(password,salt,(err,hash) => {
        console.log(hash);
    });
});

let hashPass = '$2a$10$bwmjOJ78bTDbMY45BER1wubrd9D/wJQXWSWr/53fwo1YoccLP7wxi';
bcrypt.compare(password,hashPass,(err,res) => {
    console.log(res);
});