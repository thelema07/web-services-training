/* NodeJs Modules */
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

/* Custom Routes */
const index = require('./services/routes/indexRoute');
const users = require('./services/routes/userRoute');

const app = express();

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const port = process.env.port || 4000;

// mongoose connection
mongoose.connect('mongodb://localhost:27017/crunchbase');
mongoose.connection.on('error',() => {
    console.log('Could not connect to database');
    process.exit();
});
mongoose.connection.once('open',() => {
    console.log('Succesfully connected to database');
});

app.use('/',index);
app.use('/users',users)

app.get('/',(req,res) => {
    res.sendStatus(200);
});

app.listen(port,() => {
    console.log('Server running on port: ',port);
});